/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.toko.nettigatujuhtoko.dao;

import com.machfudh.nettigatujuh.toko.nettigatujuhtoko.entity.Toko;
import java.io.Serializable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 *
 * @author Moh Machfudh
 */
public interface TokoDao extends PagingAndSortingRepository<Toko, String>{
    
}
