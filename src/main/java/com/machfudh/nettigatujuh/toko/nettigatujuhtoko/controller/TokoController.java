/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.toko.nettigatujuhtoko.controller;

import com.machfudh.nettigatujuh.toko.nettigatujuhtoko.dao.TokoDao;
import com.machfudh.nettigatujuh.toko.nettigatujuhtoko.entity.Toko;
import java.util.Date;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author Moh Machfudh
 */
@Controller
public class TokoController {
    
     public static final Logger LOGGER = LoggerFactory.getLogger(TokoController.class);
     
     @Autowired
     private TokoDao tokoDao;
     
     private Date tglsnow; 
     
     
    @PreAuthorize("hasAuthority('TOKO_VIEW')")  
    @GetMapping("/api/toko")
    @ResponseBody
    public Page<Toko> dataToko(Pageable page){
        return tokoDao.findAll(page);
    }
     
    @PreAuthorize("hasAuthority('TOKO_EDIT')")
    @PostMapping("/api/toko")
    @ResponseStatus(HttpStatus.CREATED)
    public void saveToko(@RequestBody @Valid Toko toko){
        tglsnow = new Date();
        toko.setInsertdate(Long.toString(tglsnow.getTime()));
        toko.setEditdate(Long.toString(tglsnow.getTime()));
        toko.setNotoko("TK"+Long.toString(tglsnow.getTime()));
        tokoDao.save(toko);
    } 
    
    @PreAuthorize("hasAuthority('TOKO_EDIT')")
    @PutMapping("api/toko/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void editToko(@PathVariable(name = "id") String id, @RequestBody @Valid Toko toko) {
        tglsnow = new Date();
        Toko newToko = tokoDao.findOne(id);

        if (newToko == null) {
            return;
        }
        String notoko = newToko.getNotoko();
        String usertoko = newToko.getId_user();
        String agenttoko = newToko.getId_agent();
        BeanUtils.copyProperties(toko, newToko);
        newToko.setId(id);              // tidak boleh berubah                    
        newToko.setEditdate(Long.toString(tglsnow.getTime()));
        newToko.setNotoko(notoko);   // tidak boleh berubah
        newToko.setId_user(usertoko);  // tidak boleh berubah
        newToko.setId_agent(agenttoko);  // tidak boleh berubah
        tokoDao.save(newToko);
    }
    
    
    @PreAuthorize("hasAuthority('TOKO_VIEW')")
    @GetMapping("/api/toko/{id}")
    @ResponseBody
    public Toko tokoById(@PathVariable(name = "id") String id) {
        Toko toko = tokoDao.findOne(id);
        return toko;
    }
    
    
}
