/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.machfudh.nettigatujuh.toko.nettigatujuhtoko.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Moh Machfudh
 */
@Data
@Entity @Table(name = "net_toko")
public class Toko {
    
     private static final long serialVersionUID = -7371610187321351709L;
    
    @Id @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id; 
    
    @NotNull @NotEmpty
    private String namatoko;
 
    @NotNull @NotEmpty
    private String keterangan;
    
    @NotNull @NotEmpty
    private String modeltoko;
    
    @NotNull @NotEmpty
    private String buka;
    
    @NotNull @NotEmpty
    private String tutup;
    
    @NotNull @NotEmpty
    private String statusbuka;
    
    private String phototoko;
    
    @NotNull @NotEmpty
    private String alamattoko;
    
    @NotNull @NotEmpty
    private String kodepostoko;
    
    @NotNull @NotEmpty
    private String pemilik;
    
    @NotNull @NotEmpty
    private String nohp;
    
    @NotNull @NotEmpty
    private String email;
    
    @NotNull @NotEmpty
    private String alamat;
    
    @NotNull @NotEmpty
    private String kodepos;
    
    @NotNull @NotEmpty
    private String noktp;
    
    private String nonpwp;
    
    private String noaccount;
    
    private String photopemilik; 
    
    private String photoktp;
    
    @NotNull @NotEmpty
    private String status;
    
    @NotNull @NotEmpty
    private String membership;
    
    private String catatan;
    
    private String longtitut;
    
    private String latitut;
    
    private String lastlogin;
    
    @NotNull @NotEmpty
    private String id_agent;
    
    @NotNull @NotEmpty
    private String insertid;
    
    @NotNull @NotEmpty
    private String insertdate;
    
    @NotNull @NotEmpty
    private String editid;
    
    @NotNull @NotEmpty
    private String editdate;
    
    private String id_user;
    
    @NotNull @NotEmpty
    private String notoko;
    
    
    
}
